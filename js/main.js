var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var mode = 1;
var url = "";
var url_array = new Array();
var url_n = -1;
var position_origin;
var radius = 4;
var style = 1; // 1:stroke,2:filled
var hue = 0;
window.onload = function(){
  ctx.lineWidth = 4;
  ctx.lineCap = "round";
  ctx.fillStyle = "white";
  ctx.fillRect(0,0,canvas.width,canvas.height);
  ctx.fillStyle = "black";
  url = canvas.toDataURL();
  push_url();
  console.log(1);
  document.getElementById("pencil").classList.remove("active");
};
function getmouseposition(canvas,event){
  var origin = canvas.getBoundingClientRect();
  return{
    x : event.clientX - origin.left,
    y : event.clientY - origin.top
  };
}
function mousemove(event){
  var position = getmouseposition(canvas, event);
  if(mode === 1){
    ctx.lineTo(position.x, position.y);
    ctx.stroke();
  }
  else if(mode === 2){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    loadImage();
    if(style === 1){
      ctx.strokeRect(position_origin.x, position_origin.y,
        position.x - position_origin.x, position.y - position_origin.y);
    }
    else{
      ctx.fillRect(position_origin.x, position_origin.y,
        position.x - position_origin.x, position.y - position_origin.y);
    }
  }
  else if(mode === 3){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    loadImage();
    ctx.beginPath();
    ctx.arc(position_origin.x, position_origin.y,
      Math.sqrt(Math.pow(Math.abs(position.x - position_origin.x), 2) + Math.pow(Math.abs(position.y - position_origin.y), 2)) , 
        0, 2 * Math.PI);
    if(style === 1)ctx.stroke(); 
    else ctx.fill();
  }
  else if(mode === 5){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    loadImage();
    ctx.beginPath();
    ctx.moveTo(position_origin.x, position_origin.y);
    ctx.lineTo(position_origin.x,position.y);
    ctx.lineTo(position.x,position_origin.y);
    ctx.closePath();
    if (style === 1)ctx.stroke();
    else ctx.fill();
  }
  else if(mode === 6){
    ctx.strokeStyle = `hsl(${hue}, 100%, 60%)`;
    if (hue >= 360) hue = 0;
    else hue+=2;
    ctx.lineTo(position.x, position.y);
    ctx.stroke();
    ctx.beginPath();
    ctx.moveTo(position.x,position.y);
  }
  else if(mode === 0){
    ctx.strokeStyle = "white";
    ctx.lineTo(position.x, position.y);
    ctx.stroke();
  }
}
canvas.addEventListener('mousedown',function(event){
  
  position_origin = getmouseposition(canvas,event);
  //initial color and size
  ctx.fillStyle = document.getElementById("color").value;
  ctx.strokeStyle = document.getElementById("color").value;
  if(mode === 0){
    ctx.lineWidth = radius;
  }
  else{
    ctx.lineWidth = document.getElementById("size").value;
  }
  ctx.beginPath();
  ctx.moveTo(position_origin.x, position_origin.y);
  canvas.addEventListener('mousemove',mousemove);
  //set useCaptrue = false(冒泡事件) --> from outside to inside 
  //set useCaptrue = true(capture) --> from inside to outside
  //default is false
});
canvas.addEventListener('mouseup',function(event){
  var position = getmouseposition(canvas,event);
  if (mode === 4) {
    var text = document.getElementById("text_context").value;
    ctx.fillText(text, position.x, position.y);
  }
  url = canvas.toDataURL();
  push_url();
  canvas.removeEventListener('mousemove', mousemove);  
}); 
function loadImage() {
  var img = new Image();
  img.src = url;
  ctx.drawImage(img, 0, 0);
}
function push_url() {
  url_n += 1;
  if (url_n < url_array.length) { url_array.length = url_n; }
  url_array.push(canvas.toDataURL());
}
document.getElementById("color").addEventListener('change',function(){
  ctx.strokeStyle = document.getElementById("color").value;
});
document.getElementById("pencil").addEventListener('click',function(){
  disable();
  mode = 1;
  var url_c = "url('image/pencil.cur'),pointer";
  document.getElementById("canvas").style.cursor = url_c;
  active_button();
});
document.getElementById("eraser").addEventListener('click',function(){
  disable();
  mode = 0;
  var url_c = "url('image/eraser.cur'),pointer";
  document.getElementById("canvas").style.cursor = url_c;
  active_button();
});
document.getElementById("rectangle").addEventListener('click',function () {
  disable();
  mode = 2;
  document.getElementById("canvas").style.cursor = "crosshair";
  active_button();
});
document.getElementById("circle").addEventListener('click',function(){
  disable();
  mode = 3;
  document.getElementById("canvas").style.cursor = "crosshair";
  active_button();
});
document.getElementById("text").addEventListener('click', function(){
  disable();
  mode = 4;
  ctx.font = document.getElementById("text_size").value + "px Arial";
  active_button();
});
document.getElementById("triangle").addEventListener('click',function(){
  disable();
  mode = 5;
  document.getElementById("canvas").style.cursor = "crosshair";
  active_button();
});
document.getElementById("cut").addEventListener('click', function () {
  disable();
  mode = 6;
  document.getElementById("canvas").style.cursor = "crosshair";
  active_button();
});
document.getElementById("reset").addEventListener('click',function(){
  disable();
  url_n = -1;
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.strokeStyle = document.getElementById("color").value;
  push_url();
  url = url_array[url_n];
});
document.getElementById("undo").addEventListener('click',function(){
  disable();
  if (url_n>0){
    url_n-=1;
    console.log(url_array.length);
    console.log(url_n);
    var img_undo = new Image();
    img_undo.addEventListener('load',function(){
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(img_undo, 0, 0, canvas.width, canvas.height);
    },false);
    url = url_array[url_n];
    img_undo.src = url_array[url_n];
  }
});
document.getElementById("redo").addEventListener('click', function () {
  disable();
  if(url_n<url_array.length-1){
    url_n+=1;
    console.log(url_array.length);
    console.log(url_n);
    var img_undo = new Image();
    img_undo.addEventListener('load', function () {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(img_undo, 0, 0, canvas.width, canvas.height);
    }, false);
    url = url_array[url_n];
    img_undo.src = url_array[url_n];
  }
});
document.getElementById("download_href").addEventListener('click',function(){
  this.href = document.getElementById("canvas").toDataURL();
  this.download = "painting.png";
});
document.getElementById("file").addEventListener('change',function(){
  //filelist
  var reader = new FileReader();
  var img_upload = new Image();
  if(this.files && this.files[0]){
    reader.addEventListener('load',function(event){
      img_upload.addEventListener('load',function(){
        ctx.drawImage(img_upload,0,0);
        url = img_upload.src;
        push_url();
        console.log(url_array[0]);
        console.log(url_array[1]);
        console.log(url_array.length);
        console.log(url_n);
      });
      img_upload.src = event.target.result;
    });
    reader.readAsDataURL(this.files[0]);
  }
  console.log(url_array[0]);
  console.log(url_array[1]);
  console.log(url_array.length);
  console.log(url_n);
});
document.getElementById("filled").addEventListener('click',function(){
  style = 2;
  document.getElementById("filled").classList.remove("active");
  document.getElementById("stroke").classList.add("active");
});
document.getElementById("stroke").addEventListener('click', function () {
  style = 1;
  document.getElementById("stroke").classList.remove("active");
  document.getElementById("filled").classList.add("active");
});
function disable(){
  if (mode === 0) {
    document.getElementById("eraser").classList.add("active");
  }
  else if (mode === 1) {
    document.getElementById("pencil").classList.add("active");
  }
  else if (mode === 2) {
    document.getElementById("rectangle").classList.add("active");
  }
  else if(mode === 3){
    document.getElementById("circle").classList.add("active");
  }
  else if(mode === 4){
    document.getElementById("text").classList.add("active");
  }
  else if (mode === 5) {
    document.getElementById("triangle").classList.add("active");
  }
  else if (mode === 6) {
    document.getElementById("cut").classList.add("active");
  }
}
function active_button(){
  if (mode === 0) {
    document.getElementById("eraser").classList.remove("active");
  }
  else if (mode === 1) {
    document.getElementById("pencil").classList.remove("active");
  }
  else if (mode === 2) {
    document.getElementById("rectangle").classList.remove("active");
  }
  else if (mode === 3) {
    document.getElementById("circle").classList.remove("active");
  }
  else if (mode === 4) {
    document.getElementById("text").classList.remove("active");
  }
  else if (mode === 5) {
    document.getElementById("triangle").classList.remove("active");
  }
  else if (mode === 6) {
    document.getElementById("cut").classList.remove("active");
  }
}
document.getElementById("size").addEventListener('change',function(){
  ctx.lineWidth = document.getElementById("size").value;
  document.getElementById("stroke_size_n").value = document.getElementById("size").value;
});
document.getElementById("stroke_size_n").addEventListener('change',function(){
  ctx.lineWidth = document.getElementById("stroke_size_n").value;
  document.getElementById("size").value = document.getElementById("stroke_size_n").value;
});
document.getElementById("erase_size").addEventListener('change', function () {
  radius = document.getElementById("erase_size").value;
  document.getElementById("erase_size_n").value = document.getElementById("erase_size").value;
});
document.getElementById("erase_size_n").addEventListener('change', function () {
  radius = document.getElementById("erase_size_n").value;
  document.getElementById("erase_size").value = document.getElementById("erase_size_n").value;
});