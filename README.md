# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report
本次作業的目的是為了熟悉canvas的用法，利用內建的function來製作網頁小畫家，下面會講解這次HW的實作方式
以下是我實做的功能：
1. 筆刷
2. 橡皮擦
3. 長方形、三角形、圓形
4. 顏色選擇
5. Redo、Undo 和 Reset
6. 上傳圖片及下載檔案
7. 文字(大小與內容)
8. **彩虹筆刷**
9. 實心與空心
10. 筆刷大小(筆刷及橡皮擦)

### 筆刷
首先對滑鼠設置mouseevent的監聽，對mousedown、mouseup進行偵測，並隨著滑鼠按下放開去偵測mousemove的動作
```js
canvas.addEventListener('mousedown',function(event){
  position_origin = getmouseposition(canvas,event);
  ctx.fillStyle = document.getElementById("color").value;
  ctx.strokeStyle = document.getElementById("color").value;
  if(mode === 0){
    ctx.lineWidth = radius;
  }
  else{
    ctx.lineWidth = document.getElementById("size").value;
  }
  ctx.beginPath();
  ctx.moveTo(position_origin.x, position_origin.y);
  canvas.addEventListener('mousemove',mousemove);
});
canvas.addEventListener('mouseup',function(event){
  var position = getmouseposition(canvas,event);
  url = canvas.toDataURL();
  push_url();
  if (mode === 4) {
    var text = document.getElementById("text_context").value;
    ctx.fillText(text, position.x, position.y);
  }
  canvas.removeEventListener('mousemove', mousemove);  
}); 
```
接著隨著滑鼠的移動畫下，產生筆刷的效果
```js
function mousemove(event){
    var position = getmouseposition(canvas, event);
    ctx.lineTo(position.x, position.y);
    ctx.stroke();
}
```
另外，對於滑鼠位置的取得，需要減去canvas的相對位置，好讓滑鼠位置能夠以canvas左上角為原點
```js
function getmouseposition(canvas,event){
    var origin = canvas.getBoundingClientRect();//DOMobject
    return{
        x : event.clientX - origin.left,
        y : event.clientY - origin.top
    };
}
``` 
### 橡皮擦
橡皮擦一開始的想法即是將筆刷顏色轉變成白色
```js
ctx.strokeStyle = "white";
```
但是這點會在下載圖片時出現問題，於是在一開始時便將畫布底設置為白色
```js
window.onload = function(){
  ctx.fillStyle = "white";
  ctx.fillRect(0,0,canvas.width,canvas.height);
}
```
### 長方形、圓形和三角形
利用canvas內建的function來進行繪畫，主要是對滑鼠位置做處理
```js
(mode === 2){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    loadImage();
    if(style === 1){
      ctx.strokeRect(position_origin.x, position_origin.y,
        position.x - position_origin.x, position.y - position_origin.y);
    }
    else{
      ctx.fillRect(position_origin.x, position_origin.y,
        position.x - position_origin.x, position.y - position_origin.y);
    }
}
(mode === 3){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    loadImage();
    ctx.beginPath();
    ctx.arc(position_origin.x, position_origin.y,
      Math.sqrt(Math.pow(Math.abs(position.x - position_origin.x), 2) + Math.pow(Math.abs(position.y - position_origin.y), 2)) , 
        0, 2 * Math.PI);
    if(style === 1)ctx.stroke(); 
    else ctx.fill();
}
(mode === 5){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    loadImage();
    ctx.beginPath();
    ctx.moveTo(position_origin.x, position_origin.y);
    ctx.lineTo(position_origin.x,position.y);
    ctx.lineTo(position.x,position_origin.y);
    ctx.closePath();
    if (style === 1)ctx.stroke();
    else ctx.fill();
}
```
另外繪製圖形時，為了要產生預覽動畫的效果，必須在畫之前將之前所繪製的圖形清除，因此利用了**clearRect()**
但是因為clear會將原本圖案清除，因此需要重新load

### 顏色選擇
顏色選擇是在HTML上增加input[type=color]的tag，並對input進行change的監聽，取得input的value並給到strokestyle
```js
ctx.strokeStyle = document.getElementById("color").value;
```
### Redo、Undo 和 Reset
Redo和Undo的實作中，我利用**toDataURL()**的方式去將canvas轉乘url並push到Array裡面
接著對index進行處理，取出對應Array裡面的url並繪製到canvas上

而Reset的部分，我將畫布進行清空，並重新畫上白色底的畫布，另外將Array裡面的url皆清空
### 上傳圖片與下載檔案
下載檔案的部分，我在html上設置了一個超聯結的tag，並將canvas的url設置給他的href，設置其下載內容
```js
document.getElementById("download_href").addEventListener('click',function(){
  this.href = document.getElementById("canvas").toDataURL();
  this.download = "painting.png";
});
```
上傳圖片則是利用input的tag，利用**fileReader()**物件，讓file能夠傳入並繪製，此外也要push到url的Array內
```js
var reader = new FileReader();
```
### 文字內容與大小
在html中設置input以方便使用者傳入文字內容和大小，並利用了**drawText()**來繪製Text
```js
ctx.font = document.getElementById("text_size").value + "px Arial";
var text = document.getElementById("text_context").value;
ctx.fillText(text, position.x, position.y);
```
### 彩虹筆刷
實作方式與一般筆刷差不多，唯一差別是在mousemove時不斷改變其hsl的亮度(0~360)
```js
ctx.strokeStyle = `hsl(${hue}, 100%, 60%)`;
```
### 實心與空心
這裡利用全域變數來記錄目前的狀態，接著在畫下時利用不同改變繪畫方式，即**stroke()**和**fill()**
### 筆刷大小和橡皮擦大小
利用input的range來改變其大小，在筆刷模式下和橡皮擦模式下進行值的初始化